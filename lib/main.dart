import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:prb/src/models/recipy.dart';
import 'package:prb/src/states/globalappstate.dart';
import 'package:prb/src/theme.dart';
import 'package:prb/src/widgets/customappbar.dart';
import 'package:prb/src/widgets/login.dart';
import 'package:prb/src/widgets/recipydetails/existingrecipy.dart';
import 'package:prb/src/widgets/recipydetails/newrecipy.dart';
import 'package:prb/src/widgets/recipylist.dart';
import 'package:prb/src/widgets/recipyprint.dart';
import 'package:provider/provider.dart';

void main() async {
  const parseAppId =
      String.fromEnvironment("PARSE_APP_ID", defaultValue: "myappID");
  const parseURL = String.fromEnvironment("PARSE_URL",
      defaultValue: "http://localhost:1337/parse");
  const parseClientKey = bool.hasEnvironment("PARSE_CLIENT_KEY")
      ? String.fromEnvironment("PARSE_CLIENT_KEY")
      : null;
  await Parse().initialize(
    parseAppId,
    parseURL,
    clientKey: parseClientKey,
    registeredSubClassMap: <String, ParseObjectConstructor>{
      Recipy.keyTableName: () => Recipy(),
    },
  );
  var globalAppState = GlobalAppState();
  if (await globalAppState.hasLoginToken()) {
    await globalAppState.logInWithToken();
  }
  runApp(PRBApp(globalAppState));
}

class PRBApp extends StatelessWidget {
  PRBApp(this._globalAppState);

  final GlobalAppState _globalAppState;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _globalAppState,
      child: MaterialApp.router(
        title: 'PuneeRecipyBook',
        theme: theme,
        routerDelegate: RecipyRouterDelegate(),
        routeInformationParser: RecipyRouteInformationParser(),
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', ''),
          const Locale('de', ''),
        ],
      ),
    );
  }
}

class RecipyRoutePath {
  final String? id;
  final bool isUnknown;
  final bool isPrint;
  final bool isNew;
  final bool isLogin;

  RecipyRoutePath.home()
      : id = null,
        isUnknown = false,
        isPrint = false,
        isNew = false,
        isLogin = false;

  RecipyRoutePath.details(this.id)
      : isUnknown = false,
        isPrint = false,
        isNew = false,
        isLogin = false;

  RecipyRoutePath.print(this.id)
      : isUnknown = false,
        isPrint = true,
        isNew = false,
        isLogin = false;

  RecipyRoutePath.newrecipy()
      : id = null,
        isUnknown = false,
        isPrint = false,
        isNew = true,
        isLogin = false;

  RecipyRoutePath.unknown()
      : id = null,
        isUnknown = true,
        isPrint = false,
        isNew = false,
        isLogin = false;

  RecipyRoutePath.login()
      : id = null,
        isUnknown = false,
        isPrint = false,
        isNew = false,
        isLogin = true;

  bool get isHomePage => id == null && !isNew && !isLogin;

  bool get isDetailsPage => id != null;
}

class NavigatorCallbacks {
  ValueChanged<String> handleRecipyTapped;
  ValueChanged<String> handleRecipyPrint;
  VoidCallback handleNewRecipy;
  ValueChanged<String> handleNewRecipySaved;
  VoidCallback handleDeletedRecipy;
  VoidCallback handleLoginTapped;
  VoidCallback handleLoginComplete;
  VoidCallback handleLogoutTapped;

  NavigatorCallbacks({
    required this.handleRecipyTapped,
    required this.handleRecipyPrint,
    required this.handleNewRecipy,
    required this.handleNewRecipySaved,
    required this.handleDeletedRecipy,
    required this.handleLoginTapped,
    required this.handleLoginComplete,
    required this.handleLogoutTapped,
  });
}

class RecipyRouterDelegate extends RouterDelegate<RecipyRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<RecipyRoutePath> {
  final GlobalKey<NavigatorState> navigatorKey;
  late final navigatorCallbacks = NavigatorCallbacks(
    handleRecipyTapped: _handleRecipyTapped,
    handleRecipyPrint: _handleRecipyPrint,
    handleNewRecipy: _handleNewRecipy,
    handleNewRecipySaved: _handleNewRecipySaved,
    handleDeletedRecipy: _handleDeletedRecipy,
    handleLoginTapped: _handleLoginTapped,
    handleLoginComplete: _handleLoginComplete,
    handleLogoutTapped: _handleLogoutTapped,
  );

  String? _selectedRecipyId;
  bool _isPrint = false;
  bool show404 = false;
  bool _isNew = false;
  bool _isLogin = false;
  int _listGeneration = 0;

  RecipyRouterDelegate() : navigatorKey = GlobalKey<NavigatorState>();

  @override
  RecipyRoutePath? get currentConfiguration {
    if (show404) {
      return RecipyRoutePath.unknown();
    }
    if (_isLogin) {
      return RecipyRoutePath.login();
    }
    if (_selectedRecipyId == null) {
      if (_isNew) {
        return RecipyRoutePath.newrecipy();
      }
      return RecipyRoutePath.home();
    }
    if (!_isPrint) {
      return RecipyRoutePath.details(_selectedRecipyId!);
    } else {
      return RecipyRoutePath.print(_selectedRecipyId!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Provider.value(
        value: navigatorCallbacks,
        builder: (context, _) {
          return Navigator(
            key: navigatorKey,
            pages: [
              MaterialPage(
                key: ValueKey('RecipyListPage'),
                child: Scaffold(
                  appBar: getCustomAppBar(context),
                  body: RecipyListWidget(_listGeneration),
                ),
              ),
              if (show404)
                MaterialPage(key: ValueKey("UnkownPage"), child: Text("404"))
              else if (_selectedRecipyId != null)
                RecipyDetailsPage(
                    recipyId: _selectedRecipyId!,
                    listGeneration: _listGeneration),
              if (_isPrint)
                RecipyPrintPage(
                  recipyId: _selectedRecipyId!,
                ),
              if (_isNew) RecipyNewPage(listGeneration: _listGeneration),
              if (_isLogin) LoginPage(),
            ],
            onPopPage: (route, result) {
              if (!route.didPop(result)) {
                return false;
              }

              if (_isLogin) {
                _isLogin = false;
              } else if (_isNew) {
                _isNew = false;
              } else {
                if (_isPrint) {
                  _isPrint = false;
                } else {
                  _selectedRecipyId = null;
                }
              }
              show404 = false;
              notifyListeners();

              return true;
            },
          );
        });
  }

  void _handleRecipyTapped(String recipyId) {
    _selectedRecipyId = recipyId;
    _isNew = false;
    notifyListeners();
  }

  void _handleRecipyPrint(String recipyId) {
    _selectedRecipyId = recipyId;
    _isPrint = true;
    notifyListeners();
  }

  void _handleNewRecipy() {
    _selectedRecipyId = null;
    _isPrint = false;
    _isNew = true;
    notifyListeners();
  }

  void _handleNewRecipySaved(String recipyId) {
    _isNew = false;
    _selectedRecipyId = recipyId;
    _listGeneration++;
    notifyListeners();
  }

  void _handleDeletedRecipy() {
    _selectedRecipyId = null;
    _isPrint = false;
    _listGeneration++;
    notifyListeners();
  }

  void _handleLoginTapped() {
    _isLogin = true;
    _isPrint = false;
    _isNew = false;
    _selectedRecipyId = null;
    notifyListeners();
  }

  void _handleLoginComplete() {
    _isLogin = false;
    notifyListeners();
  }

  void _handleLogoutTapped() {
    _isLogin = false;
    _isPrint = false;
    _isNew = false;
    _selectedRecipyId = null;
    notifyListeners();
  }

  @override
  Future<void> setNewRoutePath(RecipyRoutePath path) async {
    _isPrint = false;
    _isNew = false;
    _isLogin = false;
    if (path.isUnknown) {
      _selectedRecipyId = null;
      show404 = true;
      return;
    }
    if (path.isLogin) {
      _selectedRecipyId = null;
      _isLogin = true;
    }
    if (path.isNew) {
      _selectedRecipyId = null;
      _isNew = true;
    }
    if (path.isDetailsPage) {
      _selectedRecipyId = path.id!;
      if (path.isPrint) {
        _isPrint = true;
      }
    } else {
      _selectedRecipyId = null;
    }
    show404 = false;
  }
}

class RecipyRouteInformationParser
    extends RouteInformationParser<RecipyRoutePath> {
  @override
  Future<RecipyRoutePath> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = routeInformation.uri;
    // Handle '/'
    if (uri.pathSegments.length == 0) {
      return RecipyRoutePath.home();
    }

    // Handle '/newrecipy' and '/login
    if (uri.pathSegments.length == 1) {
      if (uri.pathSegments[0] == 'newrecipy') {
        return RecipyRoutePath.newrecipy();
      } else if (uri.pathSegments[0] == 'login') {
        return RecipyRoutePath.login();
      }
    }

    // Handle '/recipy/:id' and '/recipy/:id?print
    if (uri.pathSegments.length == 2) {
      if (uri.pathSegments[0] == 'recipy') {
        var id = uri.pathSegments[1];
        if (id.isEmpty) return RecipyRoutePath.unknown();
        if (uri.queryParameters.containsKey("print")) {
          return RecipyRoutePath.print(id);
        }
        return RecipyRoutePath.details(id);
      }
    }

    // Handle unknown routes
    return RecipyRoutePath.unknown();
  }

  @override
  RouteInformation? restoreRouteInformation(RecipyRoutePath path) {
    if (path.isUnknown) {
      return RouteInformation(uri: Uri.parse('/404'));
    }
    if (path.isHomePage) {
      return RouteInformation(uri: Uri.parse('/'));
    }
    if (path.isLogin) {
      return RouteInformation(uri: Uri.parse('/login'));
    }
    if (path.isNew) {
      return RouteInformation(uri: Uri.parse('/newrecipy'));
    }
    if (path.isDetailsPage) {
      var location = '/recipy/${path.id}';
      if (path.isPrint) {
        location += '?print';
      }
      return RouteInformation(uri: Uri.parse(location));
    }
    return null;
  }
}
