import 'package:flutter/material.dart';
import 'package:prb/src/states/globalappstate.dart';
import 'package:prb/src/widgets/customappbar.dart';
import 'package:prb/main.dart';
import 'package:provider/provider.dart';

class LoginPage extends Page {
  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        return LoginForm();
      },
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  void formSubmit() {
    if (_formKey.currentState!.validate()) {
      context
          .read<GlobalAppState>()
          .logIn(
            _usernameController.text,
            _passwordController.text,
          )
          .then((status) {
        if (status == LoginStatus.failed) {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text('Login Failed')));
        } else if (status == LoginStatus.success) {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text('Login Sucessfull')));
          context.read<NavigatorCallbacks>().handleLoginComplete();
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getCustomAppBar(context),
      body: Form(
        key: _formKey,
        child: Center(
          child: Container(
            width: 500,
            child: Column(children: [
              TextFormField(
                controller: _usernameController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a username';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  hintText: "Username",
                ),
              ),
              TextFormField(
                controller: _passwordController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a password';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  hintText: "Password",
                ),
                enableSuggestions: false,
                autocorrect: false,
                obscureText: true,
                onFieldSubmitted: (_) => formSubmit(),
              ),
              FilledButton(
                onPressed: formSubmit,
                child: Text('Submit'),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
