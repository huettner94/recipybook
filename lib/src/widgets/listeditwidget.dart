import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:collection/collection.dart';

class ListValueNotifier<T> extends ChangeNotifier {
  Function eq = const ListEquality().equals;

  ListValueNotifier(this._value);

  List<T> get value => _value;
  List<T> _value;
  set value(List<T> newValue) {
    if (eq(_value, newValue)) return;
    _value = newValue;
    notifyListeners();
  }

  @override
  String toString() => value.toString();
}

class ListEditingController extends ListValueNotifier<String> {
  ListEditingController(List<String> value) : super(value);

  @override
  set value(List<String> newValue) {
    if (newValue.length == 0) {
      newValue = [""];
    }
    super.value = newValue;
  }
}

class ListEditWidget extends StatefulWidget {
  final IndexedWidgetBuilder? prefixBuilder;
  final ListEditingController controller;

  const ListEditWidget({
    Key? key,
    required this.controller,
    this.prefixBuilder,
  }) : super(key: key);

  @override
  _ListEditWidgetState createState() => _ListEditWidgetState();
}

class _ListEditWidgetState extends State<ListEditWidget> {
  late List<TextEditingController> _controllers;
  late List<FocusNode> _focusNodes;

  @override
  void initState() {
    super.initState();

    _controllers = [
      for (var text in widget.controller.value) getNewController(text: text)
    ];

    _focusNodes = [for (var _ in widget.controller.value) FocusNode()];
    widget.controller.addListener(updateContent);
  }

  @override
  void didUpdateWidget(covariant ListEditWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    oldWidget.controller.removeListener(updateContent);
    updateContent();
    widget.controller.addListener(updateContent);
  }

  @override
  void dispose() {
    for (var controller in _controllers) {
      controller.dispose();
    }
    for (var focusNode in _focusNodes) {
      focusNode.dispose();
    }
    super.dispose();
  }

  TextEditingController getNewController({String? text}) {
    var controller = TextEditingController(text: text);
    controller.addListener(() {
      rebuildState();
    });
    return controller;
  }

  void updateContent() {
    for (var i = 0; i < widget.controller.value.length; i++) {
      var value = widget.controller.value[i];
      if (i < _controllers.length) {
        if (_controllers[i].text != value) {
          _controllers[i].text = value;
        }
      } else {
        _controllers.add(getNewController(text: value));
        _focusNodes.add(FocusNode());
      }
    }
    for (var i = widget.controller.value.length; i < _controllers.length; i++) {
      _controllers[i].dispose();
      _focusNodes[i].dispose();
    }
  }

  void rebuildState() {
    List<String> data = [
      for (var controller in _controllers) controller.value.text
    ];
    widget.controller.value = data;
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];

    for (var i = 0; i < _controllers.length; i++) {
      var controller = _controllers[i];

      Widget child = TextField(
          controller: controller,
          onEditingComplete:
              () {}, // This is needed to make onKey work for Enter :)
          focusNode: _focusNodes[i],
          decoration: const InputDecoration(
            isDense: true,
            border: InputBorder.none,
          ));

      _focusNodes[i].onKeyEvent = (node, event) {
        if (!(event is KeyDownEvent)) {
          return KeyEventResult.ignored;
        }
        if (event.logicalKey == LogicalKeyboardKey.backspace &&
            controller.text.isEmpty &&
            _controllers.length > 1) {
          setState(() {
            _controllers.removeAt(i);
            _focusNodes.removeAt(i);
            if (i > 0) {
              _focusNodes[i - 1].requestFocus();
            }
          });
          return KeyEventResult.handled;
        } else if (event.logicalKey == LogicalKeyboardKey.enter) {
          setState(() {
            var ctl = getNewController();
            _controllers.insert(i + 1, ctl);
            var focusNode = FocusNode();
            _focusNodes.insert(i + 1, focusNode);
            focusNode.requestFocus();
          });
          return KeyEventResult.handled;
        } else if (event.logicalKey == LogicalKeyboardKey.arrowUp) {
          if (i > 0) {
            _focusNodes[i - 1].requestFocus();
          }
          return KeyEventResult.handled;
        } else if (event.logicalKey == LogicalKeyboardKey.arrowDown) {
          if (i + 1 < _focusNodes.length) {
            _focusNodes[i + 1].requestFocus();
          }
          return KeyEventResult.handled;
        }
        return KeyEventResult.ignored;
      };

      if (widget.prefixBuilder != null) {
        var prefix = widget.prefixBuilder!(context, i);
        child = Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: prefix,
            ),
            Expanded(child: child),
          ],
        );
      }
      children.add(child);
    }

    return Column(
      children: children,
    );
  }
}
