import 'package:flutter/material.dart';
import 'package:prb/src/models/recipy.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:prb/src/models/tuple.dart';
import 'package:prb/src/widgets/customappbar.dart';
import 'package:prb/src/widgets/recipydependentwidget.dart';
import 'package:printing/printing.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class RecipyPrintPage extends Page {
  final String recipyId;

  RecipyPrintPage({
    required this.recipyId,
  }) : super();

  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        return RecipyPrintScreen(
          recipyId: recipyId,
        );
      },
    );
  }
}

class RecipyPrintScreen extends StatelessWidget {
  final String recipyId;

  RecipyPrintScreen({
    required this.recipyId,
  });

  pw.Widget _buildListItemWidget(String text) {
    return pw.Padding(
      padding: const pw.EdgeInsets.only(bottom: 10),
      child: pw.Row(
        crossAxisAlignment: pw.CrossAxisAlignment.start,
        children: [
          pw.Text("•"),
          pw.SizedBox(width: 8),
          pw.Flexible(
            child: pw.Column(
              crossAxisAlignment: pw.CrossAxisAlignment.start,
              children: [
                pw.Text(text),
              ],
            ),
          ),
        ],
      ),
    );
  }

  pw.Widget _buildListPartWidget(Tuple listpart) {
    return pw.Column(
      crossAxisAlignment: pw.CrossAxisAlignment.start,
      children: [
        pw.Padding(
          padding: const pw.EdgeInsets.only(top: 8),
          child: pw.Header(
            level: 2,
            text: listpart.item1,
          ),
        ),
        for (var inst in listpart.item2) _buildListItemWidget(inst),
      ],
    );
  }

  Future<pw.Document> _createDocument(
      BuildContext flutterContext, Recipy recipy) async {
    final doc = pw.Document();
    var universltFont = await fontFromAssetBundle("fonts/universltstd.ttf");
    var agiloHandwritingFont =
        await fontFromAssetBundle("fonts/agilohandwriting.ttf");

    doc.addPage(pw.Page(
        pageFormat: PdfPageFormat.a4,
        theme: pw.ThemeData(
          defaultTextStyle: pw.TextStyle(
            fontSize: 10,
            font: universltFont,
          ),
          header2: pw.TextStyle(
            fontWeight: pw.FontWeight.bold,
            fontSize: 12,
            font: agiloHandwritingFont,
          ),
        ),
        build: (pw.Context context) {
          return pw.Container(
            height: double.infinity,
            child: pw.Column(
              children: [
                pw.Container(
                  width: double.maxFinite,
                  alignment: pw.Alignment.center,
                  decoration: pw.BoxDecoration(
                    color: PdfColor.fromInt(Colors.black.value),
                  ),
                  child: pw.Padding(
                    padding: const pw.EdgeInsets.only(
                        top: 10, bottom: 9, left: 5, right: 5),
                    child: pw.Text(
                      recipy.title,
                      style: pw.TextStyle(
                        fontWeight: pw.FontWeight.bold,
                        fontSize: 30,
                        color: PdfColor.fromInt(Colors.white.value),
                        font: agiloHandwritingFont,
                      ),
                    ),
                  ),
                ),
                pw.SizedBox(
                  height: 15,
                ),
                pw.Padding(
                  padding: const pw.EdgeInsets.fromLTRB(7, 2, 5, 0),
                  child: pw.Row(
                    crossAxisAlignment: pw.CrossAxisAlignment.start,
                    children: [
                      pw.Expanded(
                        flex: 50,
                        child: pw.Column(
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                            if (recipy.amount != null) pw.Text(recipy.amount!),
                            for (var instruction in recipy.instructions)
                              _buildListPartWidget(instruction)
                          ],
                        ),
                      ),
                      pw.Spacer(flex: 17),
                      pw.Expanded(
                        flex: 33,
                        child: pw.DecoratedBox(
                          decoration: pw.BoxDecoration(
                            border: pw.Border.all(
                                color: PdfColor.fromInt(Colors.black.value)),
                          ),
                          child: pw.Padding(
                            padding: const pw.EdgeInsets.only(left: 10),
                            child: _buildListPartWidget(
                              Tuple(
                                  AppLocalizations.of(flutterContext)!
                                      .ingredientsList,
                                  recipy.ingredients),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                pw.SizedBox(
                  height: 4,
                ),
              ],
            ),
          );
        }));

    return doc;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getCustomAppBar(context),
      body: RecipyDependentWidget(
          recipyId: recipyId,
          builder: (context, recipy) {
            return FutureBuilder<pw.Document>(
                future: _createDocument(context, recipy),
                builder: (BuildContext context,
                    AsyncSnapshot<pw.Document> snapshot) {
                  if (snapshot.hasData) {
                    return PdfPreview(
                      canChangeOrientation: false,
                      canChangePageFormat: false,
                      initialPageFormat: PdfPageFormat.a4,
                      build: (format) => snapshot.data!.save(),
                    );
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                });
          }),
    );
  }
}
