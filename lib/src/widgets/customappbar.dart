import 'package:flutter/material.dart';
import 'package:prb/src/states/globalappstate.dart';
import 'package:prb/src/theme.dart';
import 'package:prb/main.dart';
import 'package:provider/provider.dart';

enum PopupMenuAction { login, logout }

AppBar getCustomAppBar(BuildContext context, {List<Widget>? actions}) {
  return AppBar(
    title: getAppTitle(context),
    actions: [
      if (actions != null) ...actions,
      PopupMenuButton(
        onSelected: (value) {
          var callbacks = context.read<NavigatorCallbacks>();
          if (value == PopupMenuAction.login) {
            callbacks.handleLoginTapped();
          } else if (value == PopupMenuAction.logout) {
            context
                .read<GlobalAppState>()
                .logOut()
                .then((value) => callbacks.handleLogoutTapped());
          }
        },
        itemBuilder: (context) {
          PopupMenuItem lilo;
          if (!context.read<GlobalAppState>().loggedIn) {
            lilo = PopupMenuItem(
              child: Text("Login"),
              value: PopupMenuAction.login,
            );
          } else {
            lilo = PopupMenuItem(
              child: Text("Logout"),
              value: PopupMenuAction.logout,
            );
          }
          return [lilo];
        },
      )
    ],
  );
}
