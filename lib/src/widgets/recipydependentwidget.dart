import 'package:flutter/material.dart';
import 'package:prb/src/models/recipy.dart';

typedef RecipyWidgetBuilder = Widget Function(
    BuildContext context, Recipy recipy);

class RecipyDependentWidget extends StatefulWidget {
  final String recipyId;
  final RecipyWidgetBuilder builder;

  RecipyDependentWidget({
    required this.recipyId,
    required this.builder,
  });

  @override
  _RecipyDependentWidgetState createState() => _RecipyDependentWidgetState();
}

class _RecipyDependentWidgetState extends State<RecipyDependentWidget> {
  Future<Recipy> _getRecipy() async {
    var response = await Recipy().getObject(widget.recipyId);
    if (response.success) {
      return response.result;
    } else {
      throw response.error!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Recipy>(
      future: _getRecipy(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return widget.builder(context, snapshot.data!);
        } else if (snapshot.hasError) {
          return Center(child: SelectableText(snapshot.error.toString()));
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
