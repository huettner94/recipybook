import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:image_picker/image_picker.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:prb/src/models/recipy.dart';
import 'package:prb/src/models/tuple.dart';
import 'package:prb/src/theme.dart';
import 'package:prb/src/widgets/listeditwidget.dart';

class RecipyDetailsWidget extends StatefulWidget {
  final Recipy recipy;
  final bool editing;

  RecipyDetailsWidget({
    required this.recipy,
    required this.editing,
  });

  @override
  _RecipyDetailsWidgetState createState() => _RecipyDetailsWidgetState();
}

class _RecipyDetailsWidgetState extends State<RecipyDetailsWidget> {
  void addInstruction() {
    var i = widget.recipy.instructions;
    i.add(Tuple("", []));
    setState(() {
      widget.recipy.instructions = i;
    });
  }

  @override
  Widget build(BuildContext context) {
    var mainContent;
    if (MediaQuery.of(context).orientation == Orientation.landscape) {
      mainContent = Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 50,
            child: RecipyInstructionWidget(
              editing: widget.editing,
              recipy: widget.recipy,
              addInstruction: addInstruction,
            ),
          ),
          Spacer(flex: 17),
          Expanded(
            flex: 33,
            child: RecipyIngredientsWidget(
              editing: widget.editing,
              recipy: widget.recipy,
            ),
          )
        ],
      );
    } else {
      mainContent = Column(
        children: [
          RecipyIngredientsWidget(
            editing: widget.editing,
            recipy: widget.recipy,
          ),
          RecipyInstructionWidget(
            editing: widget.editing,
            recipy: widget.recipy,
            addInstruction: addInstruction,
          ),
        ],
      );
    }
    return Container(
      height: double.infinity,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 10, right: 10, left: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 1000),
                  child: Column(
                    children: [
                      TitleWidget(
                        editing: widget.editing,
                        recipy: widget.recipy,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30, 8, 20, 0),
                        child: mainContent,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class RecipyInstructionWidget extends StatelessWidget {
  final Recipy recipy;
  final bool editing;
  final void Function() addInstruction;

  const RecipyInstructionWidget({
    required this.recipy,
    required this.editing,
    required this.addInstruction,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ToggleableEditTextWidget(
          editing: editing,
          text: recipy.amount,
          save: (value) {
            recipy.amount = value.isEmpty ? null : value;
          },
          hideonempty: true,
          hintText: AppLocalizations.of(context)!.recipyPortions,
        ),
        if (editing)
          ToggleableEditTextWidget(
            editing: editing,
            text: recipy.category,
            save: (value) {
              recipy.category = value;
            },
            hintText: AppLocalizations.of(context)!.category,
          ),
        for (var i = 0; i < recipy.instructions.length; i++)
          RecipyListPartWidget(
            recipy.instructions[i],
            editing,
            save: (value) {
              // Need to do this dance since we otherwise write to a newly "getted" list
              var inst = recipy.instructions;
              inst[i] = value;
              recipy.instructions = inst;
            },
            hintText: AppLocalizations.of(context)!.steps,
          ),
        if (editing)
          IconButton(
            padding: EdgeInsets.only(top: 10),
            icon: Icon(
              Icons.add,
              color: Theme.of(context).colorScheme.secondary,
            ),
            onPressed: addInstruction,
          ),
      ],
    );
  }
}

class RecipyIngredientsWidget extends StatelessWidget {
  final Recipy recipy;
  final bool editing;

  const RecipyIngredientsWidget({required this.recipy, required this.editing});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.only(right: 20),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.only(left: 12.0),
        child: RecipyListPartWidget(
          Tuple(AppLocalizations.of(context)!.ingredientsList,
              recipy.ingredients),
          editing,
          allowHeaderEdit: false,
          save: (value) => recipy.ingredients = value.item2,
          hintText: AppLocalizations.of(context)!.ingredients,
        ),
      ),
    );
  }
}

class RecipyListPartWidget extends StatefulWidget {
  final Tuple<String, List<String>> listpart;
  final ValueChanged<Tuple<String, List<String>>>? save;
  final bool editing;
  final bool allowHeaderEdit;
  final String? hintText;

  RecipyListPartWidget(this.listpart, this.editing,
      {this.allowHeaderEdit = true, this.save, this.hintText});

  @override
  _RecipyListPartWidgetState createState() => _RecipyListPartWidgetState();
}

class _RecipyListPartWidgetState extends State<RecipyListPartWidget> {
  final ListEditingController _controller = ListEditingController([""]);

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      _contentvalue = _controller.value;
      save();
    });
    _controller.value = widget.listpart.item2;
    _contentvalue = _controller.value;
  }

  @override
  void didUpdateWidget(covariant RecipyListPartWidget oldWidget) {
    if (!oldWidget.editing && widget.editing) {
      _controller.value = widget.listpart.item2;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void save() {
    if (widget.save != null && _headervalue != null && _contentvalue != null) {
      widget.save!(Tuple(_headervalue!, _contentvalue!));
    }
  }

  String? _headervalue;
  List<String>? _contentvalue;

  @override
  Widget build(BuildContext context) {
    var header = ToggleableEditTextWidget(
      text: widget.listpart.item1,
      save: (value) {
        _headervalue = value;
        save();
      },
      editing: widget.editing && widget.allowHeaderEdit,
      style: getHeaderStyle(context),
      hintText: AppLocalizations.of(context)!.heading,
    );

    var items;
    if (widget.editing) {
      items = [
        ListEditWidget(
          prefixBuilder: (context, index) {
            return SelectableText("•");
          },
          controller: _controller,
        ),
      ];
    } else {
      items = [
        for (var inst in widget.listpart.item2) ListItemWidget(item: inst)
      ];
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: header,
        ),
        ...items,
      ],
    );
  }
}

class ToggleableEditTextWidget extends StatefulWidget {
  final bool editing;
  final String? text;
  final ValueChanged<String>? save;
  final TextStyle? style;
  final bool hideonempty;
  late final InputDecoration inputDecoration;

  ToggleableEditTextWidget({
    required this.editing,
    required this.text,
    this.save,
    this.style,
    this.hideonempty = false,
    InputDecoration? inputDecoration,
    String? hintText,
  }) {
    if (inputDecoration != null) {
      this.inputDecoration = inputDecoration;
    } else {
      this.inputDecoration = InputDecoration(
          filled: true, fillColor: Colors.white, hintText: hintText);
    }
  }

  @override
  _ToggleableEditTextWidgetState createState() =>
      _ToggleableEditTextWidgetState();
}

class _ToggleableEditTextWidgetState extends State<ToggleableEditTextWidget> {
  final TextEditingController _controller = TextEditingController();
  @override
  void initState() {
    super.initState();
    if (widget.save != null) {
      _controller.addListener(() {
        widget.save!(_controller.text);
      });
    }
    if (widget.text != null) {
      _controller.text = widget.text!;
    }
  }

  @override
  void didUpdateWidget(covariant ToggleableEditTextWidget oldWidget) {
    if (widget.text != null) {
      _controller.text = widget.text!;
    } else {
      _controller.text = "";
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.editing) {
      return TextFormField(
        style: widget.style,
        decoration: widget.inputDecoration,
        controller: _controller,
      );
    } else {
      if (widget.hideonempty && widget.text == null) {
        return Container();
      }
      return SelectableText(
        widget.text != null ? widget.text! : "",
        style: widget.style,
      );
    }
  }
}

class ListItemWidget extends StatelessWidget {
  const ListItemWidget({
    Key? key,
    required this.item,
  }) : super(key: key);

  final String item;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("•"),
          SizedBox(width: 8),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SelectableText(item),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class EditableImageWidget extends StatefulWidget {
  final bool editing;
  final ParseFileBase? image;
  final ValueChanged<ParseFileBase> save;

  EditableImageWidget({
    required this.editing,
    required this.image,
    required this.save,
  });

  @override
  _EditableImageWidgetState createState() => _EditableImageWidgetState();
}

class _EditableImageWidgetState extends State<EditableImageWidget> {
  ParseFileBase? image;

  @override
  void initState() {
    image = widget.image;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant EditableImageWidget oldWidget) {
    image = widget.image;
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    if (image == null && !widget.editing) {
      return Container();
    }
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        if (image != null)
          FutureBuilder<ParseFileBase>(
            future: image!.download(),
            builder:
                (BuildContext context, AsyncSnapshot<ParseFileBase> snapshot) {
              if (snapshot.hasData) {
                if (kIsWeb) {
                  return Image.memory((snapshot.data as ParseWebFile).file!);
                } else {
                  return Image.file((snapshot.data as ParseFile).file!);
                }
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
        if (widget.editing)
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: ElevatedButton(
              child: Text("Set image"),
              onPressed: () async {
                XFile? pickedFile = await ImagePicker().pickImage(
                  source: ImageSource.gallery,
                  imageQuality: 25,
                  maxWidth: 1500,
                );
                if (pickedFile == null) {
                  return;
                }
                ParseFileBase parseFile;
                if (kIsWeb) {
                  //Seems weird, but this lets you get the data from the selected file as an Uint8List very easily.
                  ParseWebFile file =
                      ParseWebFile(null, name: "", url: pickedFile.path);
                  await file.download();
                  parseFile = ParseWebFile(file.file, name: "Image");
                } else {
                  parseFile = ParseFile(File(pickedFile.path));
                }
                setState(() {
                  image = parseFile;
                });
                widget.save(parseFile);
              },
            ),
          ),
      ],
    );
  }
}

class TitleWidget extends StatelessWidget {
  final bool editing;
  final Recipy recipy;

  TitleWidget({required this.editing, required this.recipy});

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        EditableImageWidget(
          editing: editing,
          image: recipy.image,
          save: (value) {
            recipy.image = value;
          },
        ),
        Padding(
          padding:
              EdgeInsets.only(top: (recipy.image != null || editing) ? 50 : 0),
          child: Container(
            width: double.maxFinite,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Theme.of(context)
                    .primaryColor
                    .withAlpha(recipy.image != null ? 200 : 255),
                border: Border.symmetric(
                    horizontal: BorderSide(
                        width: 5,
                        color: Colors.white
                            .withAlpha(recipy.image != null ? 220 : 255)))),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 20, bottom: 18, left: 20, right: 20),
              child: ToggleableEditTextWidget(
                editing: editing,
                text: recipy.title,
                save: (value) => recipy.title = value,
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 40.0,
                  color: Colors.white,
                  fontFamily: HEADER_FONT_FAMILY,
                ),
                inputDecoration: InputDecoration(
                  hintText: AppLocalizations.of(context)!.recipyname,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class RecipySaveController {
  Recipy? recipy;

  Future<String> save() async {
    assert(recipy != null);
    var instructions = recipy!.instructions;
    recipy!.instructions = instructions
        .map((e) => Tuple(e.item1.trim(),
            e.item2.map((e) => e.trim()).where((e) => e.isNotEmpty).toList()))
        .where((e) => e.item1.isNotEmpty || e.item2.isNotEmpty)
        .toList();
    var ingredients = recipy!.ingredients;
    recipy!.ingredients = ingredients.where((e) => e.isNotEmpty).toList();
    var acl = ParseACL(owner: null);
    acl.setPublicReadAccess(allowed: true);
    acl.setPublicWriteAccess(allowed: false);
    acl.setReadAccess(userId: "role:allUsers", allowed: true);
    acl.setWriteAccess(userId: "role:allUsers", allowed: true);
    recipy!.setACL(acl);
    var result = await recipy!.save();
    if (result.error != null) {
      throw Exception(result.error);
    }
    return recipy!.objectId!;
  }

  Future<void> delete() async {
    assert(recipy != null);
    await recipy!.delete();
  }

  void setRecipy(Recipy myrecipy) {
    recipy = myrecipy;
  }
}
