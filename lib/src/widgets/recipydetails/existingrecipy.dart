import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:prb/src/states/globalappstate.dart';
import 'package:prb/src/widgets/customappbar.dart';
import 'package:prb/src/widgets/recipydependentwidget.dart';
import 'package:prb/src/widgets/recipydetails/common.dart';
import 'package:prb/src/widgets/recipylist.dart';
import 'package:prb/src/theme.dart';
import 'package:prb/main.dart';
import 'package:provider/provider.dart';

class RecipyDetailsPage extends Page {
  final String recipyId;
  final int listGeneration;

  RecipyDetailsPage({
    required this.recipyId,
    required this.listGeneration,
  }) : super(key: ValueKey(recipyId));

  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        return RecipyDetailsScreen(
          recipyId: recipyId,
          listGeneration: listGeneration,
        );
      },
    );
  }
}

class RecipyDetailsScreen extends StatefulWidget {
  final String recipyId;
  final int listGeneration;

  RecipyDetailsScreen({
    required this.recipyId,
    required this.listGeneration,
  });

  @override
  _RecipyDetailsScreenState createState() => _RecipyDetailsScreenState();
}

class _RecipyDetailsScreenState extends State<RecipyDetailsScreen> {
  bool editing = false;
  RecipySaveController _controller = RecipySaveController();

  void setEditing(bool value) {
    setState(() {
      editing = value;
    });
  }

  void save() async {
    await _controller.save();
    setEditing(false);
  }

  void delete(VoidCallback handleDeletedRecipy) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(AppLocalizations.of(context)!.deleteRecipyTitle,
                    style: getHeaderStyle(context)),
                SizedBox(height: 10),
                Text(AppLocalizations.of(context)!.deleteRecipyBody(
                    _controller.recipy?.title ?? "<unknown>")),
                SizedBox(height: 10),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FilledButton(
                      child: Text(AppLocalizations.of(context)!.cancel),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    FilledButton(
                      style: FilledButton.styleFrom(
                          backgroundColor: Theme.of(context).colorScheme.error,
                          foregroundColor:
                              Theme.of(context).colorScheme.onError),
                      child: Text(AppLocalizations.of(context)!.delete,
                          style: TextStyle(
                              color: Theme.of(context).colorScheme.onError)),
                      onPressed: () async {
                        await _controller.delete();
                        handleDeletedRecipy();
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> actions = [];
    if (editing) {
      actions.add(IconButton(
        icon: Icon(Icons.delete_forever),
        onPressed: () =>
            delete(context.read<NavigatorCallbacks>().handleDeletedRecipy),
      ));
      actions.add(IconButton(
        icon: Icon(Icons.save),
        onPressed: () => save(),
      ));
      actions.add(IconButton(
        icon: Icon(Icons.edit_off),
        onPressed: () => setEditing(false),
      ));
    } else {
      if (context.watch<GlobalAppState>().loggedIn) {
        actions.add(IconButton(
          icon: Icon(Icons.edit),
          onPressed: () => setEditing(true),
        ));
      }
      actions.add(IconButton(
        icon: Icon(Icons.print),
        onPressed: () => context
            .read<NavigatorCallbacks>()
            .handleRecipyPrint(widget.recipyId),
      ));
    }

    var _recipyWidget = RecipyDependentWidget(
      recipyId: widget.recipyId,
      builder: (context, recipy) {
        _controller.setRecipy(recipy);
        return RecipyDetailsWidget(
          recipy: recipy,
          editing: editing,
        );
      },
    );

    var body;
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      body = _recipyWidget;
    } else {
      body = Row(
        children: [
          Expanded(
            flex: 5,
            child: RecipyListWidget(widget.listGeneration),
          ),
          VerticalDivider(),
          Expanded(
            flex: 15,
            child: _recipyWidget,
          ),
        ],
      );
    }

    return Scaffold(
      appBar: getCustomAppBar(context, actions: actions),
      body: body,
    );
  }
}
