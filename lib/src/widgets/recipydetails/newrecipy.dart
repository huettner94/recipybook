import 'package:flutter/material.dart';
import 'package:prb/src/models/recipy.dart';
import 'package:prb/src/models/tuple.dart';
import 'package:prb/src/widgets/customappbar.dart';
import 'package:prb/src/widgets/recipydetails/common.dart';
import 'package:prb/src/widgets/recipylist.dart';
import 'package:provider/provider.dart';
import 'package:prb/main.dart';

class RecipyNewPage extends Page {
  final int listGeneration;

  RecipyNewPage({
    required this.listGeneration,
  });

  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        return RecipyNewScreen(listGeneration: listGeneration);
      },
    );
  }
}

class RecipyNewScreen extends StatefulWidget {
  final int listGeneration;

  RecipyNewScreen({required this.listGeneration});

  @override
  _RecipyNewScreenState createState() => _RecipyNewScreenState();
}

class _RecipyNewScreenState extends State<RecipyNewScreen> {
  RecipySaveController _controller = RecipySaveController();
  late ValueChanged<String> _onSaved;

  void save() async {
    var oid = await _controller.save();
    _onSaved(oid);
  }

  @override
  Widget build(BuildContext context) {
    _onSaved = context.read<NavigatorCallbacks>().handleNewRecipySaved;

    var recipy = Recipy();
    recipy.instructions = [Tuple("", [])];
    _controller.setRecipy(recipy);
    var _recipyWidget = RecipyDetailsWidget(
      recipy: recipy,
      editing: true,
    );

    var body;
    if (MediaQuery.of(context).orientation == Orientation.portrait) {
      body = _recipyWidget;
    } else {
      body = Row(
        children: [
          Expanded(
            flex: 5,
            child: RecipyListWidget(widget.listGeneration),
          ),
          VerticalDivider(),
          Expanded(
            flex: 15,
            child: _recipyWidget,
          ),
        ],
      );
    }

    return Scaffold(
      appBar: getCustomAppBar(
        context,
        actions: [IconButton(icon: Icon(Icons.save), onPressed: save)],
      ),
      body: body,
    );
  }
}
