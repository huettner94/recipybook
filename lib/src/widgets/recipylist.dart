import 'package:flutter/gestures.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:prb/src/models/recipy.dart';
import 'package:prb/src/states/globalappstate.dart';
import 'package:prb/src/theme.dart';
import 'package:prb/main.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class SearchFocusIntent extends Intent {
  const SearchFocusIntent();
}

class RecipyListWidget extends StatefulWidget {
  final int generation;

  RecipyListWidget(this.generation);

  @override
  _RecipyListWidgetState createState() => _RecipyListWidgetState();
}

class _RecipyListWidgetState extends State<RecipyListWidget> {
  Future<List<Recipy>>? _recipyFuture;
  int _loadedGeneration = -1;

  Future<List<Recipy>> _getRecipies() async {
    var queryBuilder = QueryBuilder<Recipy>(Recipy())..setLimit(1000);
    var response = await queryBuilder.query();
    if (response.success) {
      if (response.results != null) {
        return response.results!.map((recipy) => (recipy as Recipy)).toList();
      }
      return [];
    } else {
      throw response.error!;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_recipyFuture == null || widget.generation != _loadedGeneration) {
      _recipyFuture = _getRecipies();
    }

    return FutureBuilder<List<Recipy>>(
        future: _recipyFuture!,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            _loadedGeneration = widget.generation;
            if (snapshot.hasData) {
              return RecipyListDisplayWidget(
                recipies: snapshot.data!,
                onTapped: context.read<NavigatorCallbacks>().handleRecipyTapped,
                onNewRecipy: context.read<NavigatorCallbacks>().handleNewRecipy,
              );
            } else if (snapshot.hasError) {
              return Center(child: SelectableText(snapshot.error.toString()));
            }
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }
}

class RecipyListDisplayWidget extends StatefulWidget {
  final List<Recipy> recipies;
  final ValueChanged<String> onTapped;
  final VoidCallback onNewRecipy;

  const RecipyListDisplayWidget(
      {Key? key,
      required this.recipies,
      required this.onTapped,
      required this.onNewRecipy})
      : super(key: key);

  @override
  _RecipyListDisplayWidgetState createState() =>
      _RecipyListDisplayWidgetState();
}

class _RecipyListDisplayWidgetState extends State<RecipyListDisplayWidget> {
  final TextEditingController _searchController = TextEditingController();
  final FocusNode _searchFocusNode = FocusNode();
  late List<Recipy> _filteredRecipies;

  @override
  void initState() {
    updateFilter();
    super.initState();
  }

  void updateFilter() {
    var _filter = _searchController.text;
    setState(() {
      _filteredRecipies = widget.recipies
          .where((e) => e.title.toUpperCase().contains(_filter.toUpperCase()))
          .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    var categories = _filteredRecipies.map((e) => e.category).toSet().toList()
      ..sort();
    List<Widget> children = [];

    for (var category in categories) {
      children.add(
        Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SelectableText(category, style: getHeaderStyle(context)),
              ),
              for (var recipy
                  in _filteredRecipies
                      .where((e) => e.category == category)
                      .toList()
                    ..sort())
                GestureDetector(
                  onTertiaryTapUp: (tud) => {
                    if (tud.kind == PointerDeviceKind.mouse)
                      {launch("/#/recipy/${recipy.objectId}")}
                  },
                  child: ListTile(
                    title: Text(recipy.title),
                    onTap: () => widget.onTapped(recipy.objectId!),
                  ),
                ),
            ],
          ),
        ),
      );
    }

    var mainList;
    if (children.length > 0) {
      mainList = ListView(
        children: children,
      );
    } else {
      mainList = Center(
        child: Text(AppLocalizations.of(context)!.noRecipies),
      );
    }

    return FocusableActionDetector(
      shortcuts: {
        LogicalKeySet(LogicalKeyboardKey.control, LogicalKeyboardKey.keyF):
            const SearchFocusIntent()
      },
      actions: {
        SearchFocusIntent: CallbackAction<SearchFocusIntent>(
            onInvoke: (intent) => _searchFocusNode.requestFocus())
      },
      autofocus: true,
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0, left: 8.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: TextFormField(
                focusNode: _searchFocusNode,
                controller: _searchController,
                onChanged: (value) => updateFilter(),
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context)!.recipySearch,
                ),
              ),
            ),
            Expanded(
              child: mainList,
            ),
            if (context.watch<GlobalAppState>().loggedIn)
              IconButton(
                icon: Icon(
                  Icons.add_circle,
                  color: Theme.of(context).colorScheme.primary,
                ),
                onPressed: widget.onNewRecipy,
              ),
          ],
        ),
      ),
    );
  }
}
