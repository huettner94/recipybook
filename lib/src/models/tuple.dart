// Originally taken from https://github.com/google/tuple.dart/blob/master/lib/tuple.dart

import 'package:quiver/core.dart';

class Tuple<T1, T2> {
  T1 item1;
  T2 item2;
  Tuple(this.item1, this.item2);

  factory Tuple.fromJson(List items) {
    if (items.length != 2) {
      throw ArgumentError('items must have length 2');
    }
    return Tuple<T1, T2>(items[0] as T1, items[1] as T2);
  }
  List toJson() => List.from([item1, item2], growable: false);

  @override
  String toString() => '[$item1, $item2]';

  @override
  bool operator ==(Object other) =>
      other is Tuple && other.item1 == item1 && other.item2 == item2;

  @override
  int get hashCode => hash2(item1.hashCode, item2.hashCode);
}
