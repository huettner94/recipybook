import 'dart:convert';

import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:prb/src/models/tuple.dart';

class Recipy extends ParseObject implements ParseCloneable, Comparable<Recipy> {
  static const String keyTableName = "Recipy";
  static const String _keyTitle = "Title";
  static const String _keyIngredients = "Ingredients";
  static const String _keyAmount = "Amount";
  static const String _keyInstructions = "Instructions";
  static const String _keyCategory = "Category";
  static const String _keyImage = "Image";

  Recipy() : super(keyTableName);
  Recipy.clone() : this();
  @override
  clone(Map<String, dynamic> map) => Recipy.clone()..fromJson(map);

  String get title => get<String>(_keyTitle, defaultValue: "")!;
  set title(String title) => set<String>(_keyTitle, title);

  List<String> get ingredients {
    return get<List>(_keyIngredients, defaultValue: [])!.cast<String>();
  }

  set ingredients(List<String> ingredients) =>
      set<List<String>>(_keyIngredients, ingredients);

  String? get amount => get<String>(_keyAmount);
  set amount(String? amount) => set<String?>(_keyAmount, amount);

  List<Tuple<String, List<String>>> get instructions {
    List json = jsonDecode(get<String>(_keyInstructions, defaultValue: "[]")!);
    return json
        .map((e) => [e[0], (e[1] as List).cast<String>()])
        .map((e) => Tuple<String, List<String>>.fromJson(e))
        .toList();
  }

  set instructions(List<Tuple<String, List<String>>> instructions) =>
      set<String>(_keyInstructions, jsonEncode(instructions));

  String get category => get<String>(_keyCategory, defaultValue: "")!;
  set category(String category) => set<String>(_keyCategory, category);

  ParseFileBase? get image => get<ParseFileBase>(_keyImage);
  set image(ParseFileBase? image) => set<ParseFileBase?>(_keyImage, image);

  @override
  int compareTo(Recipy other) {
    return title.compareTo(other.title);
  }
}
