import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

const HEADER_FONT_FAMILY = "AgiloHandwriting";
final mygreen = Color(0xff008000);
final myyellow = Color(0xffffffe6);

final theme = ThemeData(
  colorScheme: ColorScheme.fromSeed(
      seedColor: mygreen,
      primary: mygreen,
      surface: Colors.white,
      surfaceContainerLow: myyellow),
  appBarTheme:
      AppBarTheme(backgroundColor: mygreen, foregroundColor: Colors.white),
  fontFamily: "Universlt",
);

TextStyle getHeaderStyle(BuildContext context) {
  return TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 20.0,
    color: Theme.of(context).primaryColor,
    fontFamily: HEADER_FONT_FAMILY,
  );
}

Text getAppTitle(BuildContext context) {
  return Text(AppLocalizations.of(context)!.appTitle);
}
