import 'package:flutter/widgets.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum LoginStatus { success, failed }

const String SESSION_TOKEN_KEY = "SESSION_TOKEN_KEY";

class GlobalAppState with ChangeNotifier {
  ParseUser? _user;

  bool get loggedIn => _user != null;

  Future<void> logOut() async {
    if (_user != null) {
      await _user!.logout();
      _user = null;
      var prefs = await SharedPreferences.getInstance();
      await prefs.remove(SESSION_TOKEN_KEY);
    }
  }

  Future<bool> hasLoginToken() async {
    var prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(SESSION_TOKEN_KEY);
  }

  Future<LoginStatus> logInWithToken() async {
    if (_user != null) {
      await logOut();
    }

    var prefs = await SharedPreferences.getInstance();
    String? token;
    try {
      token = prefs.getString(SESSION_TOKEN_KEY);
    } catch (err) {
      await prefs.remove(SESSION_TOKEN_KEY);
    }
    if (token == null) {
      return LoginStatus.failed;
    }

    var user = ParseUser("", "", "", sessionToken: token);
    var response = await ParseUser.getCurrentUserFromServer(token)
        .then<LoginStatus>((response) async {
      if (response != null && response.success) {
        _user = user;
        if (user.sessionToken != null) {
          prefs.setString(SESSION_TOKEN_KEY, user.sessionToken!);
        }
        return LoginStatus.success;
      } else {
        await prefs.remove(SESSION_TOKEN_KEY);
        return LoginStatus.failed;
      }
    });
    notifyListeners();
    return response;
  }

  Future<LoginStatus> logIn(String username, String password) async {
    if (_user != null) {
      await logOut();
    }

    var prefs = await SharedPreferences.getInstance();
    await prefs.remove(SESSION_TOKEN_KEY);

    var user = ParseUser(username, password, "");
    var response = await user
        .login(doNotSendInstallationID: true)
        .then<LoginStatus>((response) async {
      if (response.success) {
        _user = user;
        if (user.sessionToken != null) {
          prefs.setString(SESSION_TOKEN_KEY, user.sessionToken!);
        }
        return LoginStatus.success;
      } else {
        return LoginStatus.failed;
      }
    });
    notifyListeners();
    return response;
  }
}
