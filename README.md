# RecipyBook

RecipyBook is a small Flutter application for sharing hosting recipies and printing them.

## Contributing

This software relies on a parse-server backend.
If you start the containers in the `docker-compose.yml` you will have enough for local development.

## License
[AGPL](LICENSE.txt)
